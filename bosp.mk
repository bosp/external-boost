
ifdef  CONFIG_EXTERNAL_BOOST

# Targets provided by this project
.PHONY: boost clean_boost

# Add this to the "external" target
external: boost
clean_external: clean_boost
dstclean_external: distclean_boost

MODULE_DIR_BOOST = $(BASE_DIR)/external/required/boost
BOOST_VERSION    = $(shell cat $(MODULE_DIR_BOOST)/index.html | grep " Release " | awk '{ print $2 }')
BOOST_LIBRARIES  = system,program_options,filesystem,regex,serialization 


B2_OPTIONS  = threading=multi
B2_OPTIONS += architecture=arm
#B2_OPTIONS += compileflags="$(TARGET_FLAGS) -fexceptions -frtti -Wno-unused-local-typedefs"
#B2_OPTIONS += linkflags="$(TARGET_FLAGS)"

ifdef CONFIG_TARGET_ANDROID
B2_OPTIONS += link=static
CMAKE_COMMON_OPTIONS += -nostdinc -I$(BASE_DIR)/toolchains/arm/bionic/lib/gcc/arm-linux-androideabi/4.9.x/include/stddef.h
endif


ifneq (,$(findstring x86_64, $(PLATFORM_TARGET)))
	B2_OPTIONS+= address-model=64
endif

JOBS:=$(shell [ $(CPUS) -gt 64 ] && echo 64 || echo $(CPUS))

boost_config:
	@echo
	@echo "==== Configuring Boost libraries [$(BOOST_VERSION)] ===="
	@cd $(MODULE_DIR_BOOST) && \
		echo "using gcc : $(GCC_VERSION) : $(CXX) : <cflags>$(TARGET_FLAGS) <cxxflags>$(TARGET_FLAGS) <linkflags>$(TARGET_FLAGS) ; " > \
			tools/build/src/user-config.jam

$(MODULE_DIR_BOOST)/b2: boost_config
	@echo
	@echo "==== Bootstrapping Boost libraries [$(BOOST_VERSION)] ===="
ifdef CONFIG_TARGET_LINUX_JETSON
	@echo "==== Bootstrap with host compiler ===="
	@cd $(MODULE_DIR_BOOST) && \
		CC=$(HOST_CC) \
		CXX=$(HOST_CXX) \
		./bootstrap.sh \
			--without-icu \
			--prefix=$(BOSP_SYSROOT)/usr \
			--with-libraries=$(BOOST_LIBRARIES) \
			exit 1
else
	@cd $(MODULE_DIR_BOOST) && \
		CC=$(CC) CFLAGS="$(TARGET_FLAGS)" \
		CXX=$(CXX) CXXFLAGS="$(TARGET_FLAGS)" \
	./bootstrap.sh \
		--without-icu \
		--prefix=$(BOSP_SYSROOT)/usr \
		--with-libraries=$(BOOST_LIBRARIES) \
		exit 1
endif

boost: setup $(MODULE_DIR_BOOST)/b2
	@echo
	@echo "==== Building Boost libraries [$(BOOST_VERSION)] ===="
	@echo " Platform     : $(PLATFORM)"
	@echo " Using CC     : $(CC)"
	@echo " Using CXX    : $(CXX)"
	@echo " Version      : $(GCC_VERSION)"
	@echo " Target flags : $(TARGET_FLAGS)"
	@echo " Sysroot      : $(BOSP_SYSROOT)"
	@echo " Machine type : $(PLATFORM_TARGET)"
	@echo " B2 Options   : $(B2_OPTIONS)"
	@echo " BOSP Options : $(CMAKE_COMMON_OPTIONS)"
	@echo " BOSP Options : $(MODULE_DIR_BOOST)"
	@cd $(MODULE_DIR_BOOST)/ && \
		./b2 --verbose -q -d0 -j$(JOBS) \
		cxxflags="$(TARGET_FLAGS) -fexceptions -frtti -Wno-unused-local-typedefs" \
		$(B2_OPTIONS) --layout=system \
		--user-config=tools/build/src/user-config.jam \
		toolset=gcc-$(GCC_VERSION) \
		target-os=linux \
		install

clean_boost:
	@echo
	@echo "==== Clean-up Boost libraries [$(BOOST_VERSION)] ===="
	@cd $(MODULE_DIR_BOOST)/ && { ! test -x ./b2 || ./b2 clean; }
	@echo

distclean_boost:
	@echo
	@echo "==== Dist-Cleanup Boost libraries [$(BOOST_VERSION)] ===="
	@cd $(MODULE_DIR_BOOST)/ && { ! test -x ./b2 || ./b2 distclean; }
	@cd $(MODULE_DIR_BOOST)/ && rm -rf `cat .gitignore`
	@echo

else # CONFIG_EXTERNAL_BOOST

boost:
	$(warning $(MODULE_DIR_BOOST) library disabled by BOSP configuration)
	$(error BOSP compilation failed)

endif # CONFIG_EXTERNAL_BOOST

